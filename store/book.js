import Vue from 'vue'

const bookServerBaseUrl = process.env.BOOK_SERVER_BASE_URL

export const state = () => ({
  books: [],
  status: {
    fetchAll: {
      loading: false,
      succeededOnce: false,
      error: null
    },
    fetchOne: {
      loading: false,
      error: null
    },
    create: {
      loading: false,
      error: null
    },
    delete: {
      loading: false,
      error: null
    }
  }
})

export const mutations = {
  fetchAllStarted (state) {
    state.status.fetchAll.loading = true
    state.status.fetchAll.error = null
  },
  fetchAllSucceeded (state, payload) {
    state.status.fetchAll.loading = false
    state.status.fetchAll.succeededOnce = true
    state.books = payload
  },
  fetchAllFailed (state, payload) {
    state.status.fetchAll.loading = false
    state.status.fetchAll.error = payload
  },
  fetchOneStarted (state) {
    state.status.fetchOne.loading = true
    state.status.fetchOne.error = null
  },
  fetchOneSucceeded (state, payload) {
    state.status.fetchOne.loading = false
    const book = state.books.find(book => book.id === payload.id)
    Vue.set(book, 'hasDetails', true)
    for (const key in payload) {
      Vue.set(book, key, payload[key])
    }
  },
  fetchOneFailed (state, payload) {
    state.status.fetchOne.loading = false
    state.status.fetchOne.error = payload
  },
  createStarted (state) {
    state.status.create.loading = true
    state.status.create.error = null
  },
  createSucceeded (state, payload) {
    state.status.create.loading = false
    state.books.unshift(payload)
  },
  createFailed (state, payload) {
    state.status.create.loading = false
    state.status.create.error = payload
  },
  deleteStarted (state) {
    state.status.delete.loading = true
    state.status.delete.error = null
  },
  deleteSucceeded (state, bookId) {
    state.status.delete.loading = false
    const index = state.books.findIndex(book => book.id === bookId)
    Vue.delete(state.books, index)
  },
  deleteFailed (state, payload) {
    state.status.delete.loading = false
    state.status.delete.error = payload
  }
}

export const actions = {
  async fetchAll ({ state, commit }) {
    if (state.status.fetchAll.succeededOnce) {
      return
    }
    commit('fetchAllStarted')
    try {
      const books = await this.$axios.$get(`${bookServerBaseUrl}/api/book`, { withCredentials: true })
      commit('fetchAllSucceeded', books)
    } catch (error) {
      commit('fetchAllFailed', error)
    }
  },
  async fetchOne ({ state, commit, dispatch }, { bookId }) {
    if (!state.status.fetchAll.succeededOnce) {
      await dispatch('fetchAll')
    }

    if (state.books.find(book => book.id === bookId).hasDetails) {
      return
    }
    commit('fetchOneStarted')
    try {
      const book = await this.$axios.$get(`${bookServerBaseUrl}/api/book/${bookId}`, { withCredentials: true })
      commit('fetchOneSucceeded', book)
    } catch (error) {
      commit('fetchOneFailed', error)
    }
  },
  async create ({ state, commit, dispatch }, payload) {
    if (!state.status.fetchAll.succeededOnce) {
      await dispatch('fetchAll')
    }
    commit('createStarted')
    try {
      const book = await this.$axios.$post(`${bookServerBaseUrl}/api/book`, payload, { withCredentials: true })
      commit('createSucceeded', book)
    } catch (error) {
      commit('createFailed', error)
    }
  },
  async delete ({ state, commit, dispatch }, payload) {
    if (!state.status.fetchAll.succeededOnce) {
      await dispatch('fetchAll')
    }
    commit('deleteStarted')
    try {
      await this.$axios.$delete(`${bookServerBaseUrl}/api/book/${payload}`, { withCredentials: true })
      commit('deleteSucceeded', payload)
    } catch (error) {
      commit('deleteFailed', error)
    }
  }
}
