const bookServerBaseUrl = process.env.BOOK_SERVER_BASE_URL

export const state = () => ({
  loginInProgress: false,
  logoutInProgress: false,
  me: null,
  message: 'none',
  error: null
})

export const mutations = {
  loginStarted (state) {
    state.loginInProgress = true
    state.message = 'Login started'
    state.error = null
  },
  loginSucceeded (state, me) {
    state.loginInProgress = false
    state.me = {
      id: me.id,
      loginName: me.loginName,
      firstName: me.firstName,
      lastName: me.lastName
    }
    state.message = 'Login succeeded'
  },
  loginFailed (state, payload) {
    state.loginInProgress = false
    state.message = 'Login failed'
    state.error = payload
  },
  logoutStarted (state) {
    state.logoutInProgress = true
    state.message = 'Logout started'
    state.error = null
  },
  logoutSucceeded (state) {
    state.logoutInProgress = false
    state.me = null
    state.message = 'Logout succeeded'
  },
  logoutFailed (state, payload) {
    state.logoutInProgress = false
    state.message = 'Logout failed'
    state.error = payload
  }
}

export const actions = {
  async login ({ commit }, { username, password }) {
    commit('loginStarted')
    const base64UsernameAndPassword = btoa(`${username}:${password}`)
    const axiosGetTokenConfig = {
      withCredentials: true,
      headers: {
        Authorization: `Basic ${base64UsernameAndPassword}`
      }
    }
    try {
      await this.$axios.$post(`${bookServerBaseUrl}/token`, undefined, axiosGetTokenConfig)
      const me = await this.$axios.$get(`${bookServerBaseUrl}/api/me`, { withCredentials: true })
      commit('loginSucceeded', me)
    } catch (error) {
      commit('loginFailed', error)
    }
  },
  async checkLogin ({ commit, state }) {
    if (!(state.loginInProgress || state.logoutInProgress) && state.me === null) {
      commit('loginStarted')
      try {
        const me = await this.$axios.$get(`${bookServerBaseUrl}/api/me`, { withCredentials: true })
        commit('loginSucceeded', me)
      } catch (error) {
        commit('loginFailed')
      }
    }
  },
  async logout ({ commit, router }) {
    commit('logoutStarted')
    try {
      await this.$axios.$delete(`${bookServerBaseUrl}/token`, { withCredentials: true })
      commit('logoutSucceeded')
    } catch (error) {
      commit('logoutFailed', error)
    }
  }
}
