export default async ({ store, redirect }) => {
  await store.dispatch('authentication/checkLogin')
  if (store.state.authentication.me) {
    return redirect('/dashboard')
  }
}
