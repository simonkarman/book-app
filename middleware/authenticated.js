export default async ({ route, store, redirect }) => {
  await store.dispatch('authentication/checkLogin')
  if (!store.state.authentication.me) {
    const { path } = route
    return redirect(encodeURI(`/login?to=${path}`))
  }
}
