import fs from 'fs'
import path from 'path'

export default {
  server: {
    host: '0.0.0.0',
    port: 17208,
    https: {
      key: fs.readFileSync(path.resolve(__dirname, '../book-server/certs/server.key')),
      cert: fs.readFileSync(path.resolve(__dirname, '../book-server/certs/server.crt'))
    }
  },
  mode: 'spa',
  env: {
    BOOK_SERVER_BASE_URL: process.env.BOOK_SERVER_BASE_URL || 'https://book.karman.dev:17209'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Don't use url loader for svg files
      const rules = config.module.rules
      for (const rule of rules) {
        if (rule.test.toString().includes('svg')) {
          // rule.exclude = '/home/skarman/projects/svg-icon-test/assets/icons/*'
          rule.test = /\.(png|jpe?g|gif|webp)$/i
        }
      }

      // Use html loader for svg files
      config.module.rules.push(
        {
          test: /\.svg$/,
          use: [{
            loader: 'html-loader',
            options: {
              minimize: true
            }
          }]
        }
      )

      // Set config to development when isDev
      if (config.isDev) { config.mode = 'development' }
    }
  }
}
